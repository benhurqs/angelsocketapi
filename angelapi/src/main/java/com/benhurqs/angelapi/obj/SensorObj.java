package com.benhurqs.angelapi.obj;

import io.realm.RealmObject;

/**
 * Created by Benhur on 12/10/16.
 */

public class SensorObj extends RealmObject {
    /**
     *

     "ecg": 45,
     "ecg_graph": 1893,
     "eda": 46,
     "eda_graph": 1893,
     "temp": 47,
     "temp_graph": 30,
     "acc_axys_x": 98,
     "acc_axys_y": -54,
     "acc_axys_z": -45

     *
     */

    public double battery;
    public double bpm;
    public double ecg;
    public double ecg_graph;
    public double eda;
    public double eda_graph;
    public double temp;
    public double temp_graph;
    public double acc_axys_x;
    public double acc_axys_y;
    public double acc_axys_z;
    public String date;
    public int dirty;

}
