package com.benhurqs.angelapi.connection;

import io.socket.client.Socket;

/**
 * Created by Benhur on 08/11/16.
 */

public class AngelAPIEvents {

    private static final String SEND_DATA = "set-events";
    private Socket socket;
    private static AngelAPIEvents instance;

    public static AngelAPIEvents getInstance(Socket socket){
        if(instance == null){
            instance = new AngelAPIEvents(socket);
        }

        return instance;
    }

    public AngelAPIEvents(Socket socket) {
        this.socket = socket;
    }

    public void sendData(String content){

    }
}
