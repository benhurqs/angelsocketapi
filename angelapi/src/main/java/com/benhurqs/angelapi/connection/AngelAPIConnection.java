package com.benhurqs.angelapi.connection;

import android.app.Activity;

import com.benhurqs.angelapi.AngelApiApplication;
import com.benhurqs.angelapi.listener.AngelConnection;

import java.io.InterruptedIOException;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by Benhur on 08/11/16.
 */

public class AngelAPIConnection {

    private Socket mSocket;
    private static AngelAPIConnection instance;
    private AngelConnection.OnAngelAPIConnectionListener listener;
    private Activity context;
    private MockJson mockJson;

    public static AngelAPIConnection getInstance(Activity context) throws InterruptedIOException{
        if(!(context.getApplication() instanceof AngelApiApplication)){
            throw new InterruptedIOException("Application not extended AngelApiApplication class");
        }

        if(instance == null){
            instance = new AngelAPIConnection(context, (AngelApiApplication) context.getApplication());
        }

        return instance;
    }

    /**
     * Init socket connection
     * @param app
     */
    public AngelAPIConnection(Activity context, AngelApiApplication app){
        this.context = context;
        mSocket = app.getSocket();
    }


    /**
     * Start connection
     */
    public void start(){
        startSocketConnection();
    }

    public void sendData(int pacient, AngelConnection.OnAngelAPISendingListener listener){
       mockJson =  MockJson.getInstance(pacient, mSocket, listener);
        mockJson.start();
    }

    /**
     * Set listener to observer connection status
     * @param listener
     * @return AngelAPIConnection instance
     */
    public AngelAPIConnection setOnConnectionStatusListener(AngelConnection.OnAngelAPIConnectionListener listener){
        this.listener = listener;
        return instance;
    }

    /**
     * Stop connection
     */
    public void stop(){
        if(mockJson != null){
            mockJson.stop();
        }
    }

    /**
     * Disconnect socket connection
     */
    public void disconnectSocket(){
        mSocket.disconnect();
        mSocket.off(Socket.EVENT_CONNECT, onConnect);
        mSocket.off(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
    }


    /**
     * Start socket connection
     */
    private void startSocketConnection(){
        mSocket.on(Socket.EVENT_CONNECT,onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT,onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.connect();
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(listener != null){
                        listener.onConnected();
                    }
                }
            });
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(listener != null) {
                        listener.onDisconnected();
                    }
                }
            });
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(listener != null) {
                        listener.onConnectionError("");
                    }

                }
            });
        }
    };

}
