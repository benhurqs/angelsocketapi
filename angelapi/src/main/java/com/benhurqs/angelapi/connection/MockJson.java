package com.benhurqs.angelapi.connection;

import android.util.Log;

import com.benhurqs.angelapi.listener.AngelConnection;
import com.benhurqs.angelapi.obj.ContentObj;
import com.benhurqs.angelapi.obj.SensorObj;
import com.benhurqs.angelapi.sensors.ACC;
import com.benhurqs.angelapi.sensors.BPM;
import com.benhurqs.angelapi.sensors.Battery;
import com.benhurqs.angelapi.sensors.ECG;
import com.benhurqs.angelapi.sensors.EDA;
import com.benhurqs.angelapi.sensors.Temp;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by Benhur on 08/11/16.
 */

public class MockJson {

    private boolean  isSending = false;
    private boolean stopIsCall = false;
    private int pacient;
    private Socket socket;
    private AngelConnection.OnAngelAPISendingListener listener;

    public static MockJson getInstance(int pacient, Socket socket, AngelConnection.OnAngelAPISendingListener listener){
        return new MockJson(pacient,socket, listener);
    }

    public MockJson(int pacient, Socket socket, AngelConnection.OnAngelAPISendingListener listener) {
        this.pacient = pacient;
        this.socket = socket;
        this.listener = listener;
    }

    public void start(){
        if (isSending) {
            return;
        }

        startThread();

    }

    public void stop(){
        stopIsCall = true;
    }

    Thread thread;

    private void startThread() {

        stopIsCall = false;
        thread = new Thread() {
            @Override
            public void run() {
                try {
                    while (socket.connected()) {
                        sleep(100);

                        sendJson(getJson(pacient));
                        isSending = true;

                        if(stopIsCall){
                            isSending = false;
                            thread.interrupt();
                            thread = null;
                            listener.onSendError(null, "Socket parado");
                            MockJson.this.stop();
                        }
                    }

                    if(!socket.connected()){
                        listener.onSendError(null, "Nao conectado");
                        MockJson.this.stop();
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        thread.start();
    }


    private void sendJson(String json) {
//        socket.emit("set-events", json, new Ack() {
//            @Override
//            public void call(Object... args) {
//                Log.e("Cheguei","aqui--->");
//            }
//        });

        Log.e("Estou enviando","aqui--->");


        socket.emit("set-events", json, new Emitter.Listener(){
            @Override
            public void call(Object... args) {
                Log.e("Cheguei","aqui--->");
            }
        });

    }

    private Emitter.Listener onSend = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("Cheguei","aqui--->");
        }
    };

    private String getJson(int pacient) {
        ACC acc = ACC.getInstance();
        Battery battery = Battery.getInstance();
        BPM bpm = BPM.getInstance();
        ECG ecg = ECG.getInstance();
        EDA eda = EDA.getInstance();
        Temp temp = Temp.getInstance();

        ContentObj contentObj = new ContentObj();
        contentObj.paciente = pacient;

        contentObj.room = 1;

        List<SensorObj> jsonObjsList = new ArrayList<SensorObj>();
        SensorObj json = new SensorObj();

        json.bpm = bpm.getValue();
        json.ecg = ecg.getValue();
        json.ecg_graph = ecg.getGraphValue();
        json.eda = eda.getValue();
        json.eda_graph = eda.getGraphValue();
        json.temp = temp.getValue();
        json.temp_graph = temp.getGraphValue();
        json.battery = battery.getValue();
        json.acc_axys_x = acc.getValue();
        json.acc_axys_y = acc.getValue();
        json.acc_axys_z = acc.getValue();

        jsonObjsList.add(json);

        Gson gson = new Gson();

        bpm.updateValues();
        ecg.updateValues();
        eda.updateValues();
        temp.updateValues();
        battery.updateValues();
        acc.updateValues();

        contentObj.content = jsonObjsList;

        listener.onSendSuccess(jsonObjsList);
        return gson.toJson(contentObj);
    }
}
