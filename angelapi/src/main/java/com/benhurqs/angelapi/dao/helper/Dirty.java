package com.benhurqs.angelapi.dao.helper;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Benhur on 11/11/16.
 */

public class Dirty {
    public static final int NEW = 0;
    public static final int SENT = 1;


    @IntDef({NEW, SENT})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Type {}
}
