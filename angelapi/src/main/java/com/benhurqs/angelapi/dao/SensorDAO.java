package com.benhurqs.angelapi.dao;

import com.benhurqs.angelapi.dao.helper.Dirty;
import com.benhurqs.angelapi.listener.OnSaveStatusListener;
import com.benhurqs.angelapi.obj.SensorObj;

import java.util.List;

import io.realm.Realm;

/**
 * Created by Benhur on 11/11/16.
 */

public class SensorDAO {

    protected Realm realm;

    public SensorDAO() {
        realm = Realm.getDefaultInstance();
    }

    /**
     * Save Sensor on DataBase
     **/
    public void save(final SensorObj sensor, final OnSaveStatusListener listener) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                //Save sensor dada local
                SensorObj newSensor = bgRealm.copyToRealm(sensor);
                newSensor.dirty = Dirty.NEW;
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                if (listener != null) {
                    listener.onSuccess();
                }
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                if (listener != null) {
                    listener.onError(error.getMessage());
                }
            }
        });
    }

    /**
     * Save Sensor on DataBase
     **/
    public void updateSensor(final List<SensorObj> sensor, final OnSaveStatusListener listener) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                //Update sensor dada local
                for(SensorObj obj : sensor){
                    findSensorByDate(bgRealm, obj.date).dirty = Dirty.SENT;
                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                if (listener != null) {
                    listener.onSuccess();
                }
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                if (listener != null) {
                    listener.onError(error.getMessage());
                }
            }
        });
    }

    /**
     * Find SensorObj by date
     * @param bgRealm
     * @param date
     * @return SensorObj
     */
    private SensorObj findSensorByDate(Realm bgRealm, String date){
        return bgRealm.where(SensorObj.class)
                .equalTo("date", date)
                .findFirst();
    }

    /**
     * Delete all data with dirty = 1
     **/
    public void deleteSentData(final OnSaveStatusListener listener) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                bgRealm.where(SensorObj.class)
                        .equalTo("dirty", Dirty.SENT)
                        .findAll()
                        .deleteAllFromRealm();
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                if (listener != null) {
                    listener.onSuccess();
                }
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                if (listener != null) {
                    listener.onError(error.getMessage());
                }
            }
        });
    }

    /**
     * Find all SensorObj with dirty = 0
     * @return List<SensorObj>
     */
    public List<SensorObj> findAllNewData(){
        return realm.copyFromRealm(
                realm.where(SensorObj.class)
                        .equalTo("dirty", Dirty.NEW)
                        .findAll());
    }



}
