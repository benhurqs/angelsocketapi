package com.benhurqs.angelapi.listener;

import com.benhurqs.angelapi.obj.SensorObj;

import java.util.List;

/**
 * Created by Benhur on 08/11/16.
 */

public class AngelConnection {

     public interface OnAngelAPIConnectionListener{
        void onConnected();
        void onDisconnected();
        void onConnectionError(String error);
    }

    public interface OnAngelAPISendingListener{
        void onSendSuccess(List<SensorObj> sensorObj);
        void onSendError(List<SensorObj> sensorObj, String error);
    }

}
