package com.benhurqs.angelapi.listener;

/**
 * Created by Benhur on 11/11/16.
 */

public interface OnSaveStatusListener {
    void onSuccess();
    void onError(String error);
}
