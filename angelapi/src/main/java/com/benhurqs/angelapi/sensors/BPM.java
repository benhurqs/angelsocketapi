package com.benhurqs.angelapi.sensors;

/**
 * Created by Benhur on 12/10/16.
 */

public class BPM extends Sensor {
    /**
     * BPM: 0 a 250, steps de 1, ao todo 250 (hoje não tem gráfico, mas to pensando em tirar o ecg e só colocar o bpm no futuro)
     */

    private static BPM instance = null;

    public static BPM getInstance(){
        if(instance == null){
            instance = new BPM();
        }

        return instance;
    }

    @Override
    double getMinValue() {
        return  0;
    }

    @Override
    double getMaxValue() {
        return 250;
    }

    @Override
    double getStepValue() {
        return 1;
    }
}
