package com.benhurqs.angelapi.sensors;

/**
 * Created by Benhur on 12/10/16.
 */

public class ACC extends Sensor {

    /**
     * ACC: -1 a +1, steps de 0,1, ao todo 20 (para cada eixo)
     **/

    private static ACC instance = null;

    public static ACC getInstance(){
        if(instance == null){
            instance = new ACC();
        }

        return instance;
    }

    @Override
    double getMinValue() {
        return -1;
    }

    @Override
    double getMaxValue() {
        return +1;
    }

    @Override
    double getStepValue() {
        return 0.1;
    }
}
