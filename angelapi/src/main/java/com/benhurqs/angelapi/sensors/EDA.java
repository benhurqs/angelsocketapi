package com.benhurqs.angelapi.sensors;

/**
 * Created by Benhur on 12/10/16.
 */

public class EDA extends Sensor {

    /**
     *

     EDA: 0 a 15 (μμS)micro Siemens, steps de 0,5, ao todo 30

     * **/

    private static EDA instance = null;

    public static EDA getInstance(){
        if(instance == null){
            instance = new EDA();
        }

        return instance;
    }

    @Override
    double getMinValue() {
        return 0;
    }

    @Override
    double getMaxValue() {
        return 15;
    }

    @Override
    double getStepValue() {
        return 0.5;
    }
}
