package com.benhurqs.angelapi.sensors;

/**
 * Created by Benhur on 12/10/16.
 */

public class Battery extends Sensor {

    /**
     * Bateria: 0 a 100 (%), steps de 1, ao todo 100
     **/

    private static Battery instance = null;

    public static Battery getInstance() {
        if (instance == null) {
            instance = new Battery();
        }

        return instance;
    }

    @Override
    double getMinValue() {
        return 0;
    }

    @Override
    double getMaxValue() {
        return 100;
    }

    @Override
    double getStepValue() {
        return 1;
    }
}