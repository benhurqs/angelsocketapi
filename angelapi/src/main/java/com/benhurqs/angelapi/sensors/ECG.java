package com.benhurqs.angelapi.sensors;

/**
 * Created by Benhur on 12/10/16.
 */

public class ECG extends Sensor {

    private static ECG instance = null;

    public static ECG getInstance(){
        if(instance == null){
            instance = new ECG();
        }

        return instance;
    }

    @Override
    double getMinValue() {
        return  -0.5;
    }

    @Override
    double getMaxValue() {
        return 1;
    }

    @Override
    double getStepValue() {
        return 0.05;
    }
}
