package com.benhurqs.angelapi.sensors;

/**
 * Created by Benhur on 12/10/16.
 */

public abstract class Sensor {

    abstract double getMinValue();
    abstract double getMaxValue();
    abstract double getStepValue();

    private double value;
    private boolean increase = true;

    public void updateValues(){
        if(value <= getMinValue()){
            increase = true;
        }else if(value >= getMaxValue()){
            increase = false;
        }

        if(increase){
            value = value + getStepValue();
        }else{
            value = value - getStepValue();
        }
    }

    public double getValue(){
        return value;
    }

    public double getGraphValue(){
        return ((value - getMinValue()) * 100)/(getMaxValue() - getMinValue());
    }

}
