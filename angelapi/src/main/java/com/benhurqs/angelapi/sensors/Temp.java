package com.benhurqs.angelapi.sensors;

/**
 * Created by Benhur on 12/10/16.
 */

public class Temp extends Sensor {

    /**
     * Temp: +35 a +42 (∘∘C), steps de 0,1, ao todo 70
     **/

    private static Temp instance = null;

    public static Temp getInstance() {
        if (instance == null) {
            instance = new Temp();
        }

        return instance;
    }

    @Override
    double getMinValue() {
        return 35;
    }

    @Override
    double getMaxValue() {
        return 42;
    }

    @Override
    double getStepValue() {
        return 0.1;
    }
}
