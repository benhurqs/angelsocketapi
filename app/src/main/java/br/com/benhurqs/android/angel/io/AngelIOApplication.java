package br.com.benhurqs.android.angel.io;

import com.benhurqs.angelapi.AngelApiApplication;

/**
 * Created by Benhur on 12/10/16.
 */

public class AngelIOApplication extends AngelApiApplication {

    @Override
    public String getUrl() {
        return "http://angelmonitor.review:3000/";
    }

}
