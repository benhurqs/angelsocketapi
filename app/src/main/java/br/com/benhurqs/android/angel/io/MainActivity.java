package br.com.benhurqs.android.angel.io;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.benhurqs.angelapi.connection.AngelAPIConnection;
import com.benhurqs.angelapi.listener.AngelConnection;
import com.benhurqs.angelapi.obj.SensorObj;

import java.io.InterruptedIOException;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AngelConnection.OnAngelAPIConnectionListener {

    //    private Socket mSocket;
    private boolean isConnected = false, isSending = false;
    private TextView txtStatus, txtEnviando;
    private Button btnConectar;
    private EditText edtPacientId;
    private AngelAPIConnection angelAPIConnection;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtStatus = (TextView) this.findViewById(R.id.txt_status);
        txtEnviando = (TextView) this.findViewById(R.id.txt_enviando);
        btnConectar = (Button) this.findViewById(R.id.btn_conectar);
        edtPacientId = (EditText) this.findViewById(R.id.edt_pacient_id);

        try {
            angelAPIConnection = AngelAPIConnection.getInstance(this)
                    .setOnConnectionStatusListener(this);
        } catch (InterruptedIOException e) {
            Log.e("error", e.getLocalizedMessage());
        }


    }

    public void onClickConectar(View v) {
        if (isEmpty(edtPacientId.getText().toString())) {
            Toast.makeText(MainActivity.this.getApplicationContext(),
                    "Preencha o valor do pacient ID", Toast.LENGTH_SHORT).show();
            return;
        }

        if (isSending) {
            angelAPIConnection.stop();
            isSending = false;
            txtStatus.setText("Parado");
            btnConectar.setText("Enviar");
            return;
        }

        if (isConnected) {
            angelAPIConnection.sendData(Integer.valueOf(edtPacientId.getText().toString()), new AngelConnection.OnAngelAPISendingListener() {
                @Override
                public void onSendSuccess(List<SensorObj> sensorObj) {
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            txtStatus.setText("Enviando");
                            btnConectar.setText("Parar");
                            isSending = true;
                        }
                    });

                }

                @Override
                public void onSendError(List<SensorObj> sensorObj, String error) {
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            isSending = false;
                            txtStatus.setText("Parado");
                            btnConectar.setText("Enviar");
                        }


                    });
                }

            }) ;
        } else {
            angelAPIConnection.start();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        angelAPIConnection.disconnectSocket();
    }

//    private static String getResourceAsString(Context context, int resourceId) {
//        try {
//            Resources res = context.getResources();
//            InputStream in_s = res.openRawResource(resourceId);
//
//            byte[] b = new byte[in_s.available()];
//            in_s.read(b);
//
//            return new String(b);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return null;
//    }

    public static boolean isEmpty(String string) {
        return (string == null || "".equals(string.trim()) || "null".equals(string.toLowerCase()));
    }


    @Override
    public void onConnected() {
        if (!isConnected) {
            txtStatus.setText("Conectado com paciente: " + edtPacientId.getText());
            btnConectar.setText("Enviar");
            Toast.makeText(MainActivity.this.getApplicationContext(),
                    "Conectado", Toast.LENGTH_LONG).show();
            isConnected = true;
        }
    }

    @Override
    public void onDisconnected() {
        txtStatus.setText("Desconectado");
        isConnected = false;
        Toast.makeText(MainActivity.this.getApplicationContext(),
                "Desconectado", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionError(String error) {
        txtStatus.setText("Error ao conectar");
        Toast.makeText(MainActivity.this.getApplicationContext(),
                "Erro na conexao", Toast.LENGTH_LONG).show();
    }
}
